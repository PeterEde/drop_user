## 0.0.2 - 08 August 2017
Fixed compatibility with Nextcloud 11 & 12.
Better JS.
Added a warning for the Nextcloud 11 session issue.

## 0.0.1 - 26th July 2017
Initial release.